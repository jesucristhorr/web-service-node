import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LocalesService } from '../../services/locales.service';

/**
 * Generated class for the ModalContentLocalEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-content-local-edit',
  templateUrl: 'modal-content-local-edit.html',
})
export class ModalContentLocalEditPage {
  local: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public localesService: LocalesService, public alertCtrl: AlertController) {
    this.local = navParams.get('local');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalContentLocalEditPage');
  }

  modificarLocal(local) {
    if(!local.id) {
      local.id = Date.now();
    }
    if(String(local.nombre).trim().charAt(0) == "" || String(local.direccion).trim().charAt(0) == "" || String(local.likes).trim().charAt(0) == "" || String(local.followers).trim().charAt(0) == "") {
      const alert = this.alertCtrl.create({
        title: 'No se modificó el local',
        subTitle: 'Por favor, rellena todos los campos.',
        buttons: ['Entiendo']
      });
      alert.present();
    } else {
      this.localesService.createLocal(local);
      console.log(local);
      const alert = this.alertCtrl.create({
        title: 'Operación realizada con éxito',
        subTitle: 'Se modificó el local correctamente.',
        buttons: ['Aceptar']
      });
      alert.present();
      this.navCtrl.pop();
    }
  }

}
