import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalContentLocalEditPage } from './modal-content-local-edit';

@NgModule({
  declarations: [
    ModalContentLocalEditPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalContentLocalEditPage),
  ],
})
export class ModalContentLocalEditPageModule {}
