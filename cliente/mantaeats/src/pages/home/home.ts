import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { PerfilPage } from '../perfil/perfil';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public imagen: any;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {
    this.verificarSesion()
  }

  ionViewWillEnter() {
    this.verificarSesion();
  }

  verificarSesion() {
    setTimeout(() => {
      this.imagen = document.getElementById('imagen');
      if(localStorage.getItem('loginData') != null) {
        let rutaImagen = JSON.parse(localStorage.loginData).user.photoURL;
        this.imagen.setAttribute("src", rutaImagen);
      } else {
        this.imagen.setAttribute("src", "http://cdn.onlinewebfonts.com/svg/img_411680.png")
      }
    }, 1)
  }

  tocarImagen() {
    if(localStorage.getItem('loginData') != null) {
      this.navCtrl.push(PerfilPage)
    } else {
      this.navCtrl.push(LoginPage)
    }
  }
}
