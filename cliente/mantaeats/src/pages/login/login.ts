import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { AuthService } from '../../services/auth.service';
import { PerfilPage } from '../perfil/perfil';
import { state, transition } from '@angular/core/src/animation/dsl';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public authService: AuthService, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  cancelar() {
    this.viewCtrl.dismiss();
  }

  logInFacebook() {
    this.authService.loginConFacebook().then((response) => {
      const alert = this.alertCtrl.create({
        title: 'Iniciar sesión',
        subTitle: 'Bienvenido, ' + response.user.displayName + '.',
        buttons: ['Aceptar']
      });
      alert.present();
      localStorage.setItem('loginData', JSON.stringify(response));
      this.viewCtrl.dismiss();
    }).catch(err => {
      const alert = this.alertCtrl.create({
        title: 'Operación no realizada',
        subTitle: 'Ocurrió un error al intentar iniciar sesión.',
        buttons: ['Aceptar']
      });
      alert.present();
      this.viewCtrl.dismiss();
    });
  }

}
