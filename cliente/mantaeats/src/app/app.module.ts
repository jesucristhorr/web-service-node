import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocalesPage } from '../pages/locales/locales';
import { InfoPage } from '../pages/info/info';
import { PerfilPage } from '../pages/perfil/perfil';
import { TabsPage } from '../pages/tabs/tabs';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ModalContentPage } from '../pages/modal-content/modal-content';
import { ModalContentLocalPage } from '../pages/modal-content-local/modal-content-local';
import { LocalesService } from '../services/locales.service';
import 'firebase/storage';
import { ModalContentLocalEditPage } from '../pages/modal-content-local-edit/modal-content-local-edit';
import { LoginPage } from '../pages/login/login';
import { AuthService } from '../services/auth.service';

export const firebaseConfig = {
  apiKey: "AIzaSyA3yK7eQeFwJZLfqcRAkgKOSABiQ5ESUaw",
  authDomain: "manta-eats.firebaseapp.com",
  databaseURL: "https://manta-eats.firebaseio.com",
  projectId: "manta-eats",
  storageBucket: "manta-eats.appspot.com",
  messagingSenderId: "746698395002"
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LocalesPage,
    InfoPage,
    PerfilPage,
    TabsPage,
    ModalContentPage,
    ModalContentLocalPage,
    ModalContentLocalEditPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LocalesPage,
    InfoPage,
    PerfilPage,
    TabsPage,
    ModalContentPage,
    ModalContentLocalPage,
    ModalContentLocalEditPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LocalesService,
    AuthService
  ],
})
export class AppModule {}
